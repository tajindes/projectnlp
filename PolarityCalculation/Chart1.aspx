﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chart1.aspx.cs" Inherits="MaanuCharting.Chart1" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NLP Project</title>
    <style>
        .centerBlock {
            position: fixed;
            left: 50%;
            width: 80%;
            height: 200px;
            margin: 0 0 0 -45%;
            text-align: center;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="centerBlock">
            <h1>NLP Assignment: Team 21</h1>
            <h2>Categorical Analysis of Movie Reviews</h2>
        </div>

        <div style="float: left; width: 400px; margin-left: 40px; margin-top: 110px">
            <p>
                Positive Cast Reviews
            <asp:Chart runat="server" Height="312px" Width="434px" ID="ChartPositive">
                <Series>
                    <asp:Series ChartArea="ChartAreaPositive" ChartType="Bar" Name="Series1">
                    </asp:Series>
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartAreaPositive" BorderDashStyle="Solid" BackColor="Azure"
                        BackSecondaryColor="White" BackGradientStyle="TopBottom">
                        <AxisX>
                            <LabelStyle Angle="90" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            </p>
        </div>
        <div style="float: left; width: 400px; margin-left: 40px; margin-top: 117px">
            <p>
                Negative Cast Reviews
                <br />
                <asp:Chart runat="server" ID="ChartNegative" Height="312px" Width="434px">
                    <Series>
                        <asp:Series ChartArea="ChartAreaNegative" ChartType="Bar" Name="Series1">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartAreaNegative" BorderDashStyle="Solid" BackColor="Azure"
                            BackSecondaryColor="White" BackGradientStyle="TopBottom">
                            <AxisX>
                                <LabelStyle Angle="90" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </p>
        </div>
        <div style="float: left; width: 400px; margin-left: 40px; margin-top: 100px">
            <p>
                OverAll Review Analysis
                <br />
                <asp:Chart runat="server" ID="ChartSentiment" Height="312px" Width="434px">
                    <Series>
                        <asp:Series ChartArea="ChartAreaSentiment" Name="Series1">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartAreaSentiment" BorderDashStyle="Solid" BackColor="Azure"
                            BackSecondaryColor="White" BackGradientStyle="TopBottom">
                            <AxisX>
                                <LabelStyle Angle="90" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </p>
        </div>
        <br style="clear: left;" />

        <div class="centerBlock">
            <h3 runat="server" id="theDiv">Movie:</h3>
        </div>
        <br style="clear: left;" />
        <br style="clear: left;" />
        <br style="clear: left;" />
        <br style="clear: left;" />
        <br style="clear: left;" />
        <br style="clear: left;" />


        <div class="centerBlock">
            <asp:Button runat="server" ID="btnPrev" OnClick="GoPrev" Text="Previous" />
            <asp:Button runat="server" ID="brnNext" OnClick="GoNext" Text="Next" />
        </div>
    </form>
</body>
</html>
