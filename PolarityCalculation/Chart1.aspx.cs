﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace MaanuCharting {
    public partial class Chart1 : System.Web.UI.Page {
        List<string> movieData;
        String movieName;
        int current_index = 0;

        public String MovieName {
            get { return movieName; }
            set { movieName = value; }
        }

        public List<string> MovieData {
            get { return movieData; }
            set { movieData = value; }
        }

        public string GetMovieDataValue(int index) {
            if (null != movieData && index < movieData.Count) {
                return movieData[index];
            }
            return string.Empty;
        }

        public void AddValueMovieData(string data) {
            if (null == movieData)
                movieData = new List<string>();
            movieData.Add(data);
        }

        private void ParseData(string data) {
            // movie:zero,pos:3,neg:1;cast1:brad pitt,pos:6,neg:2;cast2:bruce willis,pos:8,neg:2

            string[] mainTokens = data.Split(';');
            string[] subTokens;
            string[] valueTokens;

            int[] yValues = new int[2];
            string[] xValues = new string[2];
            int maxNumberCast = 0;
            string movieDisplay = "";

            if (null != mainTokens) {
                // Movie Name and sentiments chart
                subTokens = mainTokens[0].Split(',');

                if (null != subTokens) {
                    for (int j = 0; j < subTokens.Length; j++) {
                        valueTokens = subTokens[j].Split(':');

                        if (j == 0) {
                            this.movieName = valueTokens[1];
                            movieDisplay = this.movieName.ToString();
                        }
                        else {
                            yValues[j - 1] = Convert.ToInt32(valueTokens[1]);
                            xValues[j - 1] = valueTokens[0];

                        }
                    }
                }

                //Pie Chart
                ChartSentiment.Series["Series1"].Points.DataBindXY(xValues, yValues);
                ChartSentiment.Series["Series1"].ChartType = SeriesChartType.Pie;


                string[] yValuesPositive = new string[4];
                int[] xValuesPositive = new int[4];

                string[] yValuesNegative = new string[4];
                int[] xValuesNegative = new int[4];
                int xlooper;
                xlooper = 0;

                maxNumberCast = mainTokens.Length - 1;

                // Remaining Main tokens - cast members - Column Charts
                for (int i = 1; i < mainTokens.Length; i++) {
                    //cast1:brad pitt,pos:6,neg:2
                    // Movie Name and sentiments chart
                    subTokens = mainTokens[i].Split(',');

                    if (null != subTokens) {
                        for (int j = 0; j < subTokens.Length; j++) {
                            valueTokens = subTokens[j].Split(':');
                            // Cast Name
                            if (j == 0) {
                                yValuesPositive[xlooper] = valueTokens[1];
                                yValuesNegative[xlooper] = valueTokens[1];
                            }
                            // Positive
                            if (j == 1) {
                                xValuesPositive[xlooper] = Convert.ToInt32(valueTokens[1]);
                            }

                            //Negative
                            if (j == 2) {
                                xValuesNegative[xlooper] = Convert.ToInt32(valueTokens[1]);
                                xlooper++;
                            }
                        }
                    }
                }

                //Pie Chart
                ChartPositive.Series["Series1"].Points.DataBindXY(yValuesPositive,xValuesPositive );
                ChartNegative.Series["Series1"].Points.DataBindXY(yValuesNegative,xValuesNegative );



                ChartPositive.Series["Series1"].ChartType = SeriesChartType.Column;
                ChartPositive.Series["Series1"].Color = System.Drawing.Color.Green;

                ChartNegative.Series["Series1"].ChartType = SeriesChartType.Column;
                ChartNegative.Series["Series1"].Color = System.Drawing.Color.Red;

                theDiv.InnerHtml = "Movie: " + movieDisplay;

            }
        }


        protected void Page_Load(object sender, EventArgs e) {
            this.ReadFile();

            // Parse the first movie by default
            ParseData(this.GetMovieDataValue(0));
        }

        private void ReadFile() {
            StreamReader streamReader = new StreamReader("C:\\Test\\check2.txt");

            while (streamReader.EndOfStream != true) {
                this.AddValueMovieData(streamReader.ReadLine());
            }

        }

        protected void GoNext(object sender, EventArgs e)
        {
            //Method for the OnTextChanged event.
            current_index += 1;

            ParseData(this.GetMovieDataValue(current_index));
        }
        protected void GoPrev(object sender, EventArgs e)
        {
            //Method for the OnTextChanged event.
            current_index -= 1;
                current_index = 0;

            ParseData(this.GetMovieDataValue(current_index));
        }
    }
}