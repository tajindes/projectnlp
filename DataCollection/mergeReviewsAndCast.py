import sys;
import glob;
import os;
import re;
import ast;
import json;


inputfile1 = sys.argv[1];
movieCastFile = open(inputfile1, "r",errors='ignore');

inputfile2 = sys.argv[2];
movieReviewsFile = open(inputfile2, "r",errors='ignore');

outputfile = sys.argv[3];
processedMovieFile = open(outputfile, "w",errors='ignore');

reviewDictionary = movieReviewsFile.readline();
reviewDictionary = ast.literal_eval(reviewDictionary);

castInfo = movieCastFile.readline();
while(castInfo):
	try:
		castInfo = json.loads(castInfo);
	except ValueError:
		castInfo = movieCastFile.readline();
		pass;
	else:
		movieName = castInfo["originalName"];
		if movieName in reviewDictionary:
			reviewList = reviewDictionary[movieName];
		else:
			reviewList = [];
		#if "reviewsDetail" not in castInfo:
		castInfo["reviewsDetail"] = reviewList;
		#else:
		#	castInfo["reviewsDetail"].append(reviewList);

		processedMovieFile.write(str(castInfo) + "\n");
		castInfo = movieCastFile.readline();

movieCastFile.close();
movieReviewsFile.close();
processedMovieFile.close();
