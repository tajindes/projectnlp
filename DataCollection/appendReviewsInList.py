import sys;
import glob;
import os;
import re;
import json;


inputfile = sys.argv[1];
movieNamewithReviewFile = open(inputfile, "r",errors='ignore');

outputfile = sys.argv[2];
appendReviewsFile = open(outputfile, "w",errors='ignore');

line1 = movieNamewithReviewFile.readline();
movieReviewDictionary = dict();

# reviewdictionary to store movie names with number of reviews
reviewdictionary = dict();
while(line1):
	line1 = line1.strip();
	#line1 = line1.lower();
	lineList = line1.split("<====>");
	movieName = lineList[0].strip();
	movieReview = lineList[1].strip();

	if(movieName not in movieReviewDictionary):
		movieReviewDictionary[movieName] = list();
		movieReviewDictionary[movieName].append(movieReview);
		reviewdictionary[movieName] = 1;
	else:
		#print("duplicate movie is:", movieName);
		movieReviewDictionary[movieName].append(movieReview);
		reviewdictionary[movieName] = reviewdictionary[movieName] + 1;
	line1 = movieNamewithReviewFile.readline();

print ("length of the dictionary is :", str(len(movieReviewDictionary)));

'''
for key in reviewdictionary:
	print("movie name is:", key, " ---> number of reviews are:", reviewdictionary[key]);
'''

appendReviewsFile.write(str(movieReviewDictionary));

appendReviewsFile.close();
movieNamewithReviewFile.close();
