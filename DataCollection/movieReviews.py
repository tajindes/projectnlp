import sys;
import glob;
import os;
import re;

outputfile = sys.argv[2];
file2 = open(outputfile, "w");

pathvar = sys.argv[1];
dirs = os.listdir( pathvar )

for current_file in dirs:
	name = os.path.join(pathvar, current_file);
	#path = re.search('/(.+?).[0-9]+.txt', name).group(1);
	#index = path.rfind('/');
	#index = index+1;

	file1 = open(name, 'r', errors='ignore')
	lines  = file1.readlines()
	str_temp = ' '.join([line.strip() for line in lines]);

	#str_temp = re.sub('[^A-Za-z0-9\@\$_\s-]+','',str_temp);		

	file1.close();
	file2.write(str_temp +"\n");

file2.close();
