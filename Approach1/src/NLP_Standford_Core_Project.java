
import java.io.*;
import java.util.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.stanford.nlp.dcoref.CorefChain;
import edu.stanford.nlp.dcoref.CorefCoreAnnotations;
import edu.stanford.nlp.io.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentPipeline;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.*;

/** This class demonstrates building and using a Stanford CoreNLP pipeline. */
public class NLP_Standford_Core_Project {

  /** Usage: java -cp "*" StanfordCoreNlpDemo [inputFile [outputTextFile [outputXmlFile]]] */
  public static void main(String[] args) throws IOException {
	  
try {
	 // The name of the file to open.
		//String inputFileName = "dummy_processedMovieData.txt";
		String inputFileName = "evaldata.txt";
		String fileName = "dummy_movieData.txt";
    
    	JSONObject jsonObj;
    	

		// String address =
		// "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=";
		BufferedReader inputFileReader;
		
		inputFileReader = new BufferedReader(new FileReader(inputFileName));
		
		FileWriter fileWriter = new FileWriter(fileName);
		// Always wrap FileWriter in BufferedWriter.
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		
		String line;
		line = inputFileReader.readLine();
		while(line != null) {
			try {
			jsonObj = new JSONObject(line);
			
			String movieName = jsonObj.getString("name");
			String cast1Name = jsonObj.getString("cast1").toLowerCase();
			String cast2Name = jsonObj.getString("cast2").toLowerCase();
			String cast3Name = jsonObj.getString("cast3").toLowerCase();
			String cast4Name = jsonObj.getString("cast4").toLowerCase();
			JSONArray movieJsonReviews = jsonObj.getJSONArray("reviewsDetail");
			
			String [] movieReviews = null; 
			int reviewsLength = movieJsonReviews.length();
			if (reviewsLength > 0) {
				movieReviews = new String [reviewsLength];
			    for (int i = 0; i < reviewsLength; i++) {
			    	movieReviews[i] = movieJsonReviews.getString(i);
			    }
			}
			// TO DO : REPLACE \' WITH ' --> No need to do this.
			//str.split(" ");
			for(int index=0; (movieReviews != null) && (index < movieReviews.length);index++) {
				if(index == 0) {
					bufferedWriter.write("######NEW-MOVIE######" + movieName + "######REVIEW#######." + "\n");
				}
				else {
					bufferedWriter.write("######MOVIE######" + movieName + "######REVIEW#######." + "\n");
				}
				String movieReview = movieReviews[index];
				if(movieReview != null && !movieReview.isEmpty()) {
					String[] movieReviewSentences = movieReview.split("\\.");
					for (int i=0; i < movieReviewSentences.length; i++) {
						String movieSentence = movieReviewSentences[i];
						movieSentence = movieSentence + ".";
						if((!cast1Name.isEmpty() && movieSentence.indexOf(cast1Name) != -1) && (cast2Name.isEmpty() || movieSentence.indexOf(cast2Name) == -1) 
								&& (cast3Name.isEmpty() || movieSentence.indexOf(cast3Name) == -1) && (cast4Name.isEmpty() || movieSentence.indexOf(cast4Name) == -1)) {
							bufferedWriter.write("start#start " + movieSentence + "\n");
						}
						else if((cast1Name.isEmpty() || movieSentence.indexOf(cast1Name) == -1) && (!cast2Name.isEmpty() && movieSentence.indexOf(cast2Name) != -1) 
								&& (cast3Name.isEmpty() || movieSentence.indexOf(cast3Name) == -1) && (cast4Name.isEmpty() || movieSentence.indexOf(cast4Name) == -1)) {
							bufferedWriter.write("start#start " + movieSentence + "\n");
						}
						else if((cast1Name.isEmpty() || movieSentence.indexOf(cast1Name) == -1) && (cast2Name.isEmpty() || movieSentence.indexOf(cast2Name) == -1) 
								&& (!cast3Name.isEmpty() && movieSentence.indexOf(cast3Name) != -1) && (cast4Name.isEmpty() || movieSentence.indexOf(cast4Name) == -1)) {
							bufferedWriter.write("start#start " + movieSentence + "\n");
						}
						else if((cast1Name.isEmpty() || movieSentence.indexOf(cast1Name) == -1) && (cast2Name.isEmpty() || movieSentence.indexOf(cast2Name) == -1) 
								&& (cast3Name.isEmpty() || movieSentence.indexOf(cast3Name) == -1) && (!cast4Name.isEmpty() && movieSentence.indexOf(cast4Name) != -1)) {
							bufferedWriter.write("start#start " + movieSentence + "\n");
						}
						else {
							// leave that review sentence
						}
						/*
						if(i == movieReviewSentences.length-1) {
							bufferedWriter.write("#############." + "\n");
						}*/
					}
				}
			}
			line = inputFileReader.readLine();
			}
			catch(JSONException e) {
				line = inputFileReader.readLine();
				System.out.println("error occured !! - tajinder");
			}

		}
		inputFileReader.close();
		bufferedWriter.close();
		System.out.println("done!!");
	} 
	catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
/*	catch (JSONException e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
		System.out.println("JSON EXCEPTION OCCURRED - TAJINDER - " + e.getMessage());
	}
*/
	  String[] arguments = new String[2];
	  
	  arguments[0] = "-fileList";
	  arguments[1] = "dummy_movieData.txt";
//	  arguments[2] = ">";
//	  arguments[3] = "test.txt";
	  
	  System.out.println("start !!");
	  SentimentPipeline.main(arguments);
	  
	  System.out.println("success !!");
	  
	  
	  String outputFile = "outputFile.txt";
	  
	  FileWriter outputfileWriter = new FileWriter(outputFile);
	  BufferedWriter outputbufferedWriter = new BufferedWriter(outputfileWriter);
		
	  /*
	   * JSONObject jsonOutputObj;
			jsonOutputObj = new JSONObject(line);
			jsonOutputObj.remove("id");
			jsonOutputObj.remove("originalName");
			jsonOutputObj.remove("reviewsDetail");
			jsonOutputObj.remove("audienceRating");
			jsonOutputObj.remove("criticsRating");

	   */
	  
	  try {
			String inputFileName = "dummy_movieData.txt.out";
			String inputMovieFileName = "evaldata.txt";
			//String fileName = "dummy_movieData.txt";
	    
	    	JSONObject jsonObj;
	    	//JSONObject jsonOutputObj;

			// String address =
			// "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=";
			BufferedReader inputFileReader;
			
			inputFileReader = new BufferedReader(new FileReader(inputFileName));
			BufferedReader inputMovieFileReader = new BufferedReader(new FileReader(inputMovieFileName));
			List<String> movieNameList = new ArrayList<>();

			String tempString = inputMovieFileReader.readLine();
			while(tempString != null && !tempString.isEmpty()) {
				movieNameList.add(tempString);
				tempString = inputMovieFileReader.readLine();
			}
			inputMovieFileReader.close();
			
			//FileWriter fileWriter = new FileWriter(fileName);
			// Always wrap FileWriter in BufferedWriter.
			//BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			float cast1rating = 0;
			float cast1Total = 0;
			float cast2rating = 0;
			float cast2Total = 0;
			float cast3rating = 0;
			float cast3Total = 0;
			float cast4rating = 0;
			float cast4Total = 0;
			float globalCast1rating = 0;
			float globalCast2rating = 0;
			float globalCast3rating = 0;
			float globalCast4rating = 0;
			
			String line;
			line = inputFileReader.readLine();
			boolean firstTime = true;
			String cast1Name = "";
			String cast2Name = "";
			String cast3Name = "";
			String cast4Name = "";
			String movieName = "";
			
			while(line != null) {
				
				String[] tempArray = line.split("######");
				if(tempArray != null && tempArray.length > 3) {
					String movieTag = tempArray[1];
					String lastMovieName = movieName;
					movieName = tempArray[2];
				
					//String movieTempName = inputMovieFileReader.readLine();
					if(movieTag.equalsIgnoreCase("NEW-MOVIE")){
						
						if(!firstTime) {
							//System.out.println("performance: movie name is:" + lastMovieName);
							//bufferedWriter.write("######NEW-MOVIE######" + movieName + "######REVIEW#######." + "\n");
							/*System.out.println("performance: cast1 name is:"+ cast1Name + " and rating is :" + globalCast1rating);
							System.out.println("performance: cast2 name is:"+ cast2Name + " and rating is :" + globalCast2rating);
							System.out.println("performance: cast3 name is:"+ cast3Name + " and rating is :" + globalCast3rating);
							System.out.println("performance: cast4 name is:"+ cast4Name + " and rating is :" + globalCast4rating + "\n");*/
							double rating1 = (1000.0 * globalCast1rating) / 9.0;
							double rating2 = (1000.0 * globalCast2rating) / 9.0;
							double rating3 = (1000.0 * globalCast3rating) / 9.0;
							double rating4 = (1000.0 * globalCast4rating) / 9.0;
							
							JSONObject jsonOutput = new JSONObject();
							jsonOutput.putOnce("movieName", lastMovieName);
							jsonOutput.putOnce("cast1Name", cast1Name);
							jsonOutput.put("cast1Rating", rating1);
							jsonOutput.putOnce("cast2Name", cast2Name);
							jsonOutput.put("cast2Rating", rating2);
							jsonOutput.putOnce("cast3Name", cast3Name);
							jsonOutput.put("cast3Rating", rating3);
							jsonOutput.putOnce("cast4Name", cast4Name);
							jsonOutput.put("cast4Rating", rating4);
							outputbufferedWriter.write(jsonOutput.toString() + "\n");
							jsonOutput = null;
						}
						else if(firstTime){
							firstTime = false;
						}

						for(int j=0; j < movieNameList.size(); j++) {
							String movieTempName = movieNameList.get(j);
							jsonObj = new JSONObject(movieTempName);
							String nameFromJson = jsonObj.getString("name");
							if(movieName.equalsIgnoreCase(nameFromJson)) {
								cast1Name = jsonObj.getString("cast1").toLowerCase();
								cast2Name = jsonObj.getString("cast2").toLowerCase();
								cast3Name = jsonObj.getString("cast3").toLowerCase();
								cast4Name = jsonObj.getString("cast4").toLowerCase();
								
								cast1rating = 0;
								cast1Total = 0;
								cast2rating = 0;
								cast2Total = 0;
								cast3rating = 0;
								cast3Total = 0;
								cast4rating = 0;
								cast4Total = 0;
								globalCast1rating = 0;
								globalCast2rating = 0;
								globalCast3rating = 0;
								globalCast4rating = 0;
								break;
							}
							//movieTempName = inputMovieFileReader.readLine();
						}
					}
					else {
						if(cast1rating > 0){
						globalCast1rating = globalCast1rating + cast1rating/cast1Total;
						cast1rating = 0;
						}
/*						else if(cast1rating < 0) {
							globalCast1rating--;
							cast1rating = 0;
						}
*/						if(cast2rating > 0){
						globalCast2rating = globalCast2rating + cast2rating/cast2Total;
						cast2rating = 0;
						}
/*						else if(cast2rating < 0) {
							globalCast2rating--;
							cast2rating = 0;
						}
*/						if(cast3rating > 0){
						globalCast3rating = globalCast3rating + cast3rating/cast3Total;
						cast3rating = 0;
						}
/*						else if(cast3rating < 0) {
							globalCast3rating--;
							cast3rating = 0;
						}
*/						if(cast4rating > 0){
						globalCast4rating = globalCast4rating + cast4rating/cast4Total;
						cast4rating = 0;
						}
/*						else if(cast4rating < 0) {
							globalCast4rating--;
							cast4rating = 0;
						}
*/					}
					line = inputFileReader.readLine();
					continue;
				}
				if(line.length() > 11 && line.substring(0, 11).equalsIgnoreCase("start#start")){
					if((!cast1Name.isEmpty() && line.indexOf(cast1Name) != -1) && (cast2Name.isEmpty() || line.indexOf(cast2Name) == -1) 
							&& (cast3Name.isEmpty() || line.indexOf(cast3Name) == -1) && (cast4Name.isEmpty() || line.indexOf(cast4Name) == -1)) {
						line = inputFileReader.readLine().trim();
						cast1Total = cast1Total + 10;
						//if(line.equalsIgnoreCase("positive") || line.equalsIgnoreCase("very positive")){
						if(line.equalsIgnoreCase("positive")){
							cast1rating = cast1rating + 7;
						}
						else if (line.equalsIgnoreCase("very positive")) {
							cast1rating = cast1rating + 9;
						}
						else if(line.equalsIgnoreCase("negative")){
							cast1rating = cast1rating + 4;
						}
						else if(line.equalsIgnoreCase("very negative")){
							cast1rating = cast1rating + 2;
						}
						else if(line.equalsIgnoreCase("neutral")){
							cast1rating = cast1rating + 6;
						}
						else {
							System.out.println("some thing is wrong - check it - tajinder");
						}
						line = inputFileReader.readLine();
					}
					else if((cast1Name.isEmpty() || line.indexOf(cast1Name) == -1) && (!cast2Name.isEmpty() && line.indexOf(cast2Name) != -1) 
							&& (cast3Name.isEmpty() || line.indexOf(cast3Name) == -1) && (cast4Name.isEmpty() || line.indexOf(cast4Name) == -1)) {
						line = inputFileReader.readLine().trim();
						cast2Total = cast2Total + 10;
						if(line.equalsIgnoreCase("positive")){
							cast2rating = cast2rating + 7;
						}
						else if (line.equalsIgnoreCase("very positive")) {
							cast2rating = cast2rating + 9;
						}
						else if(line.equalsIgnoreCase("negative")){
							cast2rating = cast2rating + 4;
						}
						else if(line.equalsIgnoreCase("very negative")){
							cast2rating = cast2rating + 2;
						}
						else if(line.equalsIgnoreCase("neutral")){
							cast2rating = cast2rating + 6;
						}
						else {
							System.out.println("some thing is wrong - check it - tajinder");
						}
						line = inputFileReader.readLine();
					}
					else if((cast1Name.isEmpty() || line.indexOf(cast1Name) == -1) && (cast2Name.isEmpty() || line.indexOf(cast2Name) == -1) 
							&& (!cast3Name.isEmpty() && line.indexOf(cast3Name) != -1) && (cast4Name.isEmpty() || line.indexOf(cast4Name) == -1)) {
						line = inputFileReader.readLine().trim();
						cast3Total = cast3Total + 10;
						if(line.equalsIgnoreCase("positive")){
							cast3rating = cast3rating + 7;
						}
						else if (line.equalsIgnoreCase("very positive")) {
							cast3rating = cast3rating + 9;
						}
						else if(line.equalsIgnoreCase("negative")){
							cast3rating = cast3rating + 4;
						}
						else if(line.equalsIgnoreCase("very negative")){
							cast3rating = cast3rating + 2;
						}
						else if(line.equalsIgnoreCase("neutral")){
							cast3rating = cast3rating + 6;
						}
						else {
							System.out.println("some thing is wrong - check it - tajinder");
						}
						line = inputFileReader.readLine();
					}
					else if((cast1Name.isEmpty() || line.indexOf(cast1Name) == -1) && (cast2Name.isEmpty() || line.indexOf(cast2Name) == -1) 
							&& (cast3Name.isEmpty() || line.indexOf(cast3Name) == -1) && (!cast4Name.isEmpty() && line.indexOf(cast4Name) != -1)) {
						line = inputFileReader.readLine().trim();
						cast4Total = cast4Total + 10;
						if(line.equalsIgnoreCase("positive")){
							cast4rating = cast4rating + 7;
						}
						else if (line.equalsIgnoreCase("very positive")) {
							cast4rating = cast4rating + 9;
						}
						else if(line.equalsIgnoreCase("negative")){
							cast4rating = cast4rating + 4;
						}
						else if(line.equalsIgnoreCase("very negative")){
							cast4rating = cast4rating + 2;
						}
						else if(line.equalsIgnoreCase("neutral")){
							cast4rating = cast4rating + 6;
						}
						else {
							System.out.println("some thing is wrong - check it - tajinder");
						}
						line = inputFileReader.readLine();
					}
					else {
						line = inputFileReader.readLine();
						// leave that review sentence
					}
				}
				else {
					line = inputFileReader.readLine();
				}
			}
			inputFileReader.close();
			outputbufferedWriter.close();

	  }
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
}